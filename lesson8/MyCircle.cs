﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyCircle
{
    public partial class MyCircle : Form
    {
        private TextBox circleDiameterTextBox;
        private Button createCircleButton;
        private CheckBox filledcheckbox;
        private Circle _myCircle;

        public MyCircle()
        {
            InitializeComponent();
            // Create new Circle object, and assign to class member property.
            _myCircle = new Circle();

        }


        private void InitializeComponent()
        {
            this.createCircleButton = new System.Windows.Forms.Button();
            this.circleDiameterTextBox = new System.Windows.Forms.TextBox();
            this.filledcheckbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // createCircleButton
            // 
            this.createCircleButton.Location = new System.Drawing.Point(186, 21);
            this.createCircleButton.Name = "createCircleButton";
            this.createCircleButton.Size = new System.Drawing.Size(75, 23);
            this.createCircleButton.TabIndex = 1;
            this.createCircleButton.Text = "Draw";
            this.createCircleButton.UseVisualStyleBackColor = true;
            this.createCircleButton.Click += new System.EventHandler(this.createCircleButton_Click);
            // 
            // circleDiameterTextBox
            // 
            this.circleDiameterTextBox.Location = new System.Drawing.Point(43, 21);
            this.circleDiameterTextBox.Name = "circleDiameterTextBox";
            this.circleDiameterTextBox.Size = new System.Drawing.Size(127, 20);
            this.circleDiameterTextBox.TabIndex = 2;
            // 
            // filledcheckbox
            // 
            this.filledcheckbox.AutoSize = true;
            this.filledcheckbox.Location = new System.Drawing.Point(43, 47);
            this.filledcheckbox.Name = "filledcheckbox";
            this.filledcheckbox.Size = new System.Drawing.Size(50, 17);
            this.filledcheckbox.TabIndex = 3;
            this.filledcheckbox.Text = "Filled";
            this.filledcheckbox.UseVisualStyleBackColor = true;
            // 
            // MyCircle
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.filledcheckbox);
            this.Controls.Add(this.circleDiameterTextBox);
            this.Controls.Add(this.createCircleButton);
            this.Name = "MyCircle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void createCircleButton_Click(object sender, EventArgs e)
        {
            // Variable for TryParse result.
            int diameter = 0;

            // Try to convert the entered text to an integer, and assign result to
            // Circle diameter property using class set accessor.
            // If the TryParse method fails, diameter will be 0.
            if (Int32.TryParse(circleDiameterTextBox.Text, out diameter) == true)
            {
                _myCircle.Diameter = diameter;
                // Output text to Studio console so we can see what happened.
                Console.WriteLine("New diameter is: {0}", diameter);
                // Get a graphics canvas to draw on, and call the DrawCircle public method
                // of the Circle class to draw the actual red circle.
                if (filledcheckbox.Checked)
                {                    
                    _myCircle.DrawFilledCircle(CreateGraphics(), 10, 10);
                }
                else
                {
                    _myCircle.DrawCircle(CreateGraphics(), 10, 10);
                }

            }
            else
            {
                // Output text to Studio console so we can see what happened.
                Console.WriteLine("Non-integer entry: {0}", circleDiameterTextBox.Text);
            }


        }



       


    }
}
