﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;   // Added to gain access to the Pen object

namespace MyCircle
{
    class Circle
    {
        // ** Properties **
        // Private member property _circleDiameter, with default of 0.
        private int _circleDiameter = 0;
        private Color _circleColor = System.Drawing.Color.Red;


        // ** Accessors **
        // Public accessors for getting and setting member property _circleDiameter.
        public int Diameter
        {
            // Sets the _circleDiameter. The value variable is the 
            // value the user specified when calling the accessor.
            set
            {
                _circleDiameter = value;
            }
            // Returns the current value of _circleDiameter.
            get
            {
                return _circleDiameter;
            }
        }

        // ** Public methods **
        // Public method (behavior) to draw a circle.
        public void DrawCircle(Graphics drawingArea, int x, int y)
        {
            // Make sure the drawingArea is valid.
            if (drawingArea != null)
            {
                // Call the DrawEllipse method of the drawing area,
                // creating a dynamic Pen for a color, using the x
                // and y coordinates to specify starting point at the
                // top-left corner, and using the m_intDiameter
                // member property for both the height and width.                
                drawingArea.DrawEllipse(new System.Drawing.Pen(_circleColor), x, y, _circleDiameter, _circleDiameter);
                
            }
        }

        public void DrawFilledCircle(Graphics drawingArea, int x, int y)
        {
            // Make sure the drawingArea is valid.
            if (drawingArea != null)
            {
                // Call the DrawEllipse method of the drawing area,
                // creating a dynamic Pen for a color, using the x
                // and y coordinates to specify starting point at the
                // top-left corner, and using the m_intDiameter
                // member property for both the height and width.                
                drawingArea.FillEllipse(new SolidBrush(_circleColor), x, y, _circleDiameter, _circleDiameter);

            }
        }
    }

}  

